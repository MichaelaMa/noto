// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    apiKey: "AIzaSyAmlJ-HTd7i2TlZJI4EoxDnlxyl-9rK1Jo",
    authDomain: "noto-fc6d3.firebaseapp.com",
    databaseURL: "https://noto-fc6d3-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "noto-fc6d3",
    storageBucket: "noto-fc6d3.appspot.com",
    messagingSenderId: "621291219053",
    appId: "1:621291219053:web:e23c32f5c2a7356fed3764",
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
