import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {TodoDialogComponent, TodoDialogResult} from "../todo-dialog/todo-dialog.component";
import {Todo} from "../entities/todo";
import {Observable} from "rxjs";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-todopage',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.scss', './../app.component.scss']
})
export class TodoPageComponent {

  constructor(private dialog: MatDialog, private store: AngularFirestore, private authService: AuthService) {}

  newTodo(): void {
    if(!this.authService.uuid)
      return;
    const dialogRef = this.dialog.open(TodoDialogComponent, {
      width: '270px',
      data: {
        todo: {},
      },
    });
    dialogRef
      .afterClosed()
      .subscribe((result: TodoDialogResult|undefined) => {
        if (!result) {
          return;
        }
        result.todo.checked=false;
        this.store.collection(this.authService.getUserId()).add(result.todo);
      });
  }

  editTask(todo: Todo): void {
    if(!this.authService.uuid)
      return;

    const dialogRef = this.dialog.open(TodoDialogComponent, {
      width: '270px',
      data: {
        todo,
        enableDelete: true,
      },
    });
    dialogRef.afterClosed().subscribe((result: TodoDialogResult|undefined) => {
      if (!result) {
        return;
      }
      if (result.delete) {
        this.store.collection(this.authService.getUserId()).doc(todo.id).delete();
      } else {
        this.store.collection(this.authService.getUserId()).doc(todo.id).update(todo);
      }
    });
  }

  todos = this.store.collection(this.authService.getUserId()).valueChanges({idField: 'id'}) as Observable<Todo[]>;

}
