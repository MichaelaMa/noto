import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Todo} from "../entities/todo";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {MatCheckboxChange} from "@angular/material/checkbox";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  @Input() todo: Todo | null = null;
  @Output() edit = new EventEmitter<Todo>();

  constructor(private store: AngularFirestore, private authService: AuthService) { }

  ngOnInit(): void {
  }

  checkedChanged(event: MatCheckboxChange): void{
    if(this.todo!=null){
      this.todo!.checked=event.checked
      this.store.collection(this.authService.getUserId()).doc(this.todo!.id).update(this.todo!);
    }
  }

}
