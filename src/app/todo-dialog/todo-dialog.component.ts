import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Todo} from "../entities/todo";

@Component({
  selector: 'app-todo-dialog',
  templateUrl: './todo-dialog.component.html',
  styleUrls: ['./todo-dialog.component.scss']
})
export class TodoDialogComponent {

  private backupTodo: Partial<Todo> = { ...this.data.todo };

  constructor(
    public dialogRef: MatDialogRef<TodoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TodoDialogData
  ) {}

  cancel(): void {
    this.data.todo.text = this.backupTodo.text;
    this.data.todo.checked = this.backupTodo.checked;
    this.dialogRef.close(this.data);
  }
}

export interface TodoDialogData {
  todo: Partial<Todo>;
  enableDelete: boolean;
}

export interface TodoDialogResult {
  todo: Todo;
  delete?: boolean;
}
