import {Injectable, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AngularFireAuth} from "@angular/fire/compat/auth";

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit{

  userLoggedIn: boolean;
  uuid: null | string;

  constructor(private router: Router, private auth: AngularFireAuth) {
    this.userLoggedIn = false;
    this.uuid = null;

    this.auth.currentUser.then((user) => {
      if (user) {
        this.userLoggedIn = true;
        this.uuid = user.uid
      } else {
        this.userLoggedIn = false;
        this.uuid = null;
      }
    });

    this.auth.onAuthStateChanged((user) => {
      if (user) {
        this.userLoggedIn = true;
        this.uuid = user.uid
      } else {
        this.userLoggedIn = false;
        this.uuid = null;
      }
    });
  }

  async ngOnInit(): Promise<void> {
    let user = (await this.auth.currentUser)
    if (user) {
      this.userLoggedIn = true;
      this.uuid = user.uid
    } else {
      this.userLoggedIn = false;
      this.uuid = null;
    }
  }

  login(user: any): Promise<any> {
    return this.auth.signInWithEmailAndPassword(user.mail, user.password)
      .then(() => {
        console.log('Auth Service: loginUser: success');
      })
      .catch(error => {
        console.log('Auth Service: login error...');
        console.log('error code', error.code);
        console.log('error', error);
        return { isValid: false, message: error.message };
      });
  }

  signupUser(user: any): Promise<any> {
    return this.auth.createUserWithEmailAndPassword(user.mail, user.password)
      .then((result) => {
        if(result.user!=null){
          result.user.sendEmailVerification();
        }
      })
      .catch(error => {
        console.log('Auth Service: signup error', error);
        return { isValid: false, message: error.message };
      });
  }

  logout(): void {
    this.auth.signOut().then(r => {
      this.userLoggedIn = false;
      this.router.navigate(['/login'])
    });
  }

  getUserId(): string {
    return <string>this.uuid;
  }

}
