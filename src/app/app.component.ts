import { Component } from '@angular/core';
import {AuthService} from "./services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'noto';

  constructor(private authService: AuthService, router: Router) {}

  checkLoggedIn(): boolean {
    return this.authService.userLoggedIn;
  }

  logout(): void {
    this.authService.logout();
  }
}
