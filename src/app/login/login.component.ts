import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', './../app.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    'mail': new FormControl('', [Validators.required, Validators.email]),
    'password': new FormControl('', Validators.required),
  });
  firebaseErrorMessage: string;

  constructor(private authService: AuthService, private router: Router) {
    this.firebaseErrorMessage = '';
  }

  ngOnInit(): void {
  }

  login() {
    if (this.loginForm==null || this.loginForm!.invalid)
      return;

    this.authService.login(this.loginForm.value).then((result) => {
      if (result == null)
        this.router.navigate(['/todopage']);
      else if (result.isValid == false)
        this.firebaseErrorMessage = result.message;
    }).catch(() => {

    });
  }

}
